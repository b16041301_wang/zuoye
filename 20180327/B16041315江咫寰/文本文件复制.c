#include<stdio.h>
#include<stdlib.h>
int main(int argc,char **argv){
    FILE *fp1,*fp2;
    char ch;
    if(argc-3){
        printf("");
        exit(1);
    }
    fp1=fopen(argv[1],"r");
    if(!fp1){
        printf("fail to open file1");
        exit(1);
    }
    fp2=fopen(argv[2],"w");
    if(!fp2){
        printf("fail to open file2");
        exit(1);
    }
    while((ch=fgetc(fp1))!=EOF){

        fputc(ch,fp2);
    }
    fclose(fp1);
    fclose(fp2);
}
