#include<stdio.h>
#include<stdlib.h>
int main(int agrc,char **agrv)
{
    FILE *fp1 = (agrv[1],"r");
    FILE *fp2 = (agrv[2],"w");
    int ch;
    if(fp1 == NULL||fp2 == NULL)
    {
        printf("FILE CANNOT FOUND");
        return 0;
    }
    ch = fgetc(fp1);
    while(ch!=EOF)
    {
        fputc(ch,fp2);
        ch = fgetc(fp1);
    }
    fclose(fp1);
    fclose(fp2);
    return 0;
}
