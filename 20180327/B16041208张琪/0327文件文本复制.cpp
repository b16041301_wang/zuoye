#include<stdio.h>
#include<stdlib.h>
int main(int argc,char * argv[], char * envp[])
{
  FILE* srcfile = 0, *destfile =0;
  int ch = 0;
  int i = 0;
  if (argc != 3)
  {
    printf("Usage:%s src-file-name dest-file-name/n",argv[0]);
  }
  else
  {
    if((srcfile = fopen(argv[1],"r")) == 0)
	{
      printf("Can not open source file %s !",argv[1]);
    }
    else
    {
      if((destfile = fopen(argv[2],"w")) ==0)
	  {
        printf("Can not open destination file %s !",argv[2]);
	  }
	  else
	  {
        while((ch = fgetc(srcfile))!= EOF) fputc(ch,destfile);
        printf("Successful to copy a file!/n");
        fclose(srcfile);
        fclose(destfile);
        printf("%d command line parameters are got in program /n",argc);
		printf("All command line parameters are list here:/n");
		while(envp[i]!=NULL)
		{
		  printf("%s/n",argv[i]); 
		  i++;
        }
		i = 0;
        printf("The variable set is list here:/n");
		while(envp[i]!=NULL)
		{
		printf("%s/n",envp[i]);
		i++;
		}
		return 0;
      }
    }
  }
return 1;
}
