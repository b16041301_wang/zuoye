char* symmetric(int size, char** matrix)
{
    assert(size > 0 && matrix != NULL);
    int num = 1;
    for (int row = 0; row < size; row++)
    {
        for (int col = row; col < size; col++)
        {
            matrix[row][col] = matrix[col][row] = num++;
        }
    }
    return matrix;
}
