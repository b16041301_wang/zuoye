#include <iostream>  
#include <cstring>  
using namespace std;  
char crack(char ch)  
{  
    int n;  
    if (ch>='a'&&ch<='z'){  
        ch -= ('a'-'A');  
        n = ch-'A';  
        n = (n+3)%26;  
        ch = n+'A';  
    }  
    else{  
        ch -= ('A'-'a');  
        n = ch-'a';  
        n = (n+3)%26;  
        ch = n+'a';  
    }  
    return ch;  
}  
int main()  
{  
    int j=0;  
    string original, cryptograph;  
    cin >> cryptograph;  
    original = cryptograph;  
    for (long i=cryptograph.size()-1; i>=0; i--){  
        original[j++] = crack(cryptograph[i]);  
    }  
    cout << original;  
    return 0;  
} 
