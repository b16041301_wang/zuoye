#include<stdio.h>
#include<stdlib.h>
#include<string.h>
char *solvethree(char * c)
{
	char t;
	int i; 
	while(c[i]!='\0'){
		t=(c[i]+3-'a')%26+'a';
		c[i]=t;
		i++;
	}
	return c;	
}
char *solvebig(char *c){
	int i=0;
	char t;
	while(c[i]!='\0'){
		if(c[i]<='Z')
			t=c[i]+32;
		else
			t=c[i]-32;
		c[i]=t;
		i++;
	}
	return c;
}
char *solvebackset(char *c){
	int i=0;
	char t;
	while(c[i]!='\0'){
		i++;
	}
	int j,n=i-1;
	for(j=0;j<i/2;j++){
		t=c[j];
		c[j]=c[n];
		c[n]=t;
		n--;
	}
	return c;
}
int main(){
	char c[20];
	gets(c);
	solvethree(c);
	solvebig(c);
	solvebackset(c);
	puts(c);
} 
