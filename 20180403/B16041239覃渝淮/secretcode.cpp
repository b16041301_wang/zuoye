#include<stdio.h>
#include<string>
#include<iostream>
using namespace std;
string rev(string str)
{
    int n=str.length()/2;
    int i=0;
    char tmp=0;
    for(i=0;i<n;i++)
    {
        tmp=str[i];
        str[i]=str[n-i-1];
        str[n-i-1]=tmp;
    }
    return str;
}
int main()
{
    string in;
    int i=0;
    cout<<"请输入长度小于50且只包含大小写字母的字符串"<<endl;
    cin>>in;
    rev(in);
    for(int i=0;i<=in.length();i++)
    {
		if('a'<=in[i]&&in[i]<='z')
            in[i]=(in[i]+23-'a')%26+'A';
		else if(in[i]<='Z'&&in[i]>='A')
            in[i]=(in[i]-'A'+23)%26+'a';
    }
    cout<<in<<endl;
}
