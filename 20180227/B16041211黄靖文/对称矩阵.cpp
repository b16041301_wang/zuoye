//
//  main.cpp
//  对称矩阵.cpp
//
//  Created by 黄靖文 on 2018/3/20.
//  Copyright © 2018年 黄靖文. All rights reserved.
//

#include <iostream>
using namespace std;

int Print(int arr[][4], int r)
{
    int i,j,z = 0;
    for(i = 0; i < r; i++)
    {
        if(z == r)
        {
            cout<<""<<endl;
            z = 0;
        }
        for(j = 0; j < r; j++)
        {
            cout<<arr[i][j]<<" ";
            z++;
        }
    }
    return 0;
}



int main() {
    int v=1,i,j,a[4][4];
    for (i=0;i<4;i++)
    {
        for (j=i;j<=3;j++)
        {
            a[i][j]=v;
            v++;
            a[j][i]=a[i][j];
        }
    }
    Print(a, 4);
    return 0;
}



