#include<stdio.h>

int Prime(int x)
{
	if(x<2)
	  return 0;
	int i;
	for(i=2;i*i<=x;i++)
	   if(x%i==0)
		  return 0;
	return 1;
}

int main()
{
	int n,count=0;
	for(n=1;n<=1000;n++)
	{
        if(Prime(n))
		{
		      printf("%5d",n);
		      count++;
		      if(!(count%5))
	          {
		         printf("\n");
		         continue;
	          }
        }
    }
    return 0;
}
